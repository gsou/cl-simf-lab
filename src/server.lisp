;; Copyright (C) 2022 gsou
;;
;; This file is part of cl-simf-lab.
;;
;; cl-simf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; cl-simf-lab is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with cl-simf-lab.  If not, see <https://www.gnu.org/licenses/>.

(in-package :cl-simf-lab)


(defpsmacro make-dataset (title labl data)
  `(create
    type    "line"
    data    (create
             labels ,labl
             datasets (array (create
                              label ,title
                              background-color "rgb(255, 99, 132)"
                              border-color     "rgb(255, 99, 132)"
                              data ,data
                              )))
    options (create)))


(defun on-new-window (body)
  (js-execute body (ps (setf clog_debug t)))
  ;; Init plugins
  (init-chart body)
  (clog-gui-initialize body)
  ;; Create index page
  (setf (title (html-document body)) "CL-SIMF Lab")
  (clog:with-clog-create body
      (gui-menu-bar ()
                    (gui-menu-drop-down (:content "Tools")
                                        ;; (gui-menu-item (:content "Open Datafile" :on-click 'on-file-open-datafile))
                                        (gui-menu-item (:content "New Simulation..." :on-click 'on-file-new-simulation))
                                        (gui-menu-item (:content "Simulation Monitor..." :on-click 'on-file-open-monitor))
                                        (gui-menu-item (:content "Tracer Viewer..." :on-click 'on-file-open-tracer))
                                        ;; (gui-menu-item (:content "Test Chart" :on-click 'on-test-steeve))
                                        )
                    (gui-menu-drop-down (:content "SLY")
                                        (gui-menu-item (:bind but-copy :content "Copy BODY to emacs"))))
    (set-on-click-copy but-copy "BODY" body)
    ;; (setf (width p3) "100%")
    ;; (setf (height p3) "100")
    ;; (set-border p3 :thin :solid :black)
    ;; (setf (editablep p3) t)
    ;; (set-on-click gen
    ;;               (lambda (obj)
    ;;                 (js-execute obj
    ;;                             (ps
    ;;                               (progn
    ;;                                 ((@ (clog-chart/chart chart) data datasets push)
    ;;                                  (create
    ;;                                   label "Precipications"
    ;;                                   background-color "rgb(255, 99, 132)"
    ;;                                   border-color     "rgb(255, 99, 132)"
    ;;                                   data (lisp (read-from-string (clog:inner-html p3)))))
    ;;                                 (setf
    ;;                                  (@ (clog-chart/chart chart) data labels)
    ;;                                  (lisp #("January" "February" "March" "April" "May" "June" "July"))))))
    ;;                 (clog-chart/update chart)))
    ))

(defun start ()
  (initialize 'on-new-window
              :static-root (merge-pathnames "./static-files/"
                                            (asdf:system-source-directory :cl-simf-lab)))
  ;; (open-browser)
  )
