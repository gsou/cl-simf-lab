;; Copyright (C) 2022 gsou
;;
;; This file is part of cl-simf-lab.
;;
;; cl-simf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; cl-simf-lab is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with cl-simf-lab.  If not, see <https://www.gnu.org/licenses/>.

(in-package :cl-simf-lab-utils)

(defmacro ps! (obj &body body) `(js-execute ,obj (ps ,@body)))

(defmacro ps-> (&body body) (let ((s (gensym))) `(lambda (,s) (ps! ,s ,@body))))

(defmacro with-clog-window ((obj &key (title "New Window")) (&rest menu) (&rest content) &body body)
  `(with-clog-create (clog-gui:window-content (clog-gui:create-gui-window ,obj :title ,title))
       (div ()
            (gui-menu-bar () ,@menu)
            ,@content)
       ,@body))

(defmacro set-on-click-ps (button &body body)
  `(set-on-click ,button (ps-> ,@body)))

(defmacro set-on-click-obj (button &body body)
  `(set-on-click ,button (lambda (obj) (declare (ignorable obj)) ,@body)))

(defmacro set-on-click-copy (button blurb &rest objects)
  (let ((s (gensym)))
    `(set-on-click ,button (lambda (,s) (declare (ignore ,s))
                             (slynk-mrepl:copy-to-repl-in-emacs (list ,@objects) :blurb ,blurb)))))

(defmacro format-emacs (fmt &rest args)
  "Run the given forms in emacs"
  ;; with-current-buffer (window-buffer)
  `(uiop:run-program (list "emacsclient" "-n" "--eval" (format nil ,fmt ,@args))))
