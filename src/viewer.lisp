;; Copyright (C) 2022 gsou
;;
;; This file is part of cl-simf-lab.
;;
;; cl-simf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; cl-simf-lab is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with cl-simf-lab.  If not, see <https://www.gnu.org/licenses/>.

(in-package :cl-simf-lab)

;;; Element views

(defgeneric element/has-view (element)
  (:documentation "This method returns whether an element has a view."))
(defmethod element/has-view ((element element)) nil)
(defgeneric element/view (element menu content)
  (:documentation "This method creates the view for the current window"))

(defun create-element-view-window (obj element)
  "Create the view window of an ELEMENT"
  (if (element/has-view element)
      (let* ((win-content (clog-gui:window-content
                           (clog-gui:create-gui-window obj
                                                       :title (element/name element)))))
        (with-clog-create win-content
            (div (:bind content)
                 (gui-menu-bar (:bind menu)))
          ;; TODO Create default element menu items
          (element/view element menu content)))
      (alert-toast obj "Invalid Element" (format nil "The element ~S does not have a view" element) :time-out 3.5)))

;;; Gui windows

;; (defun on-load-datafile (obj fname)
;;   "Open the visualiser for the given filename"

;;   )

(defun on-file-open-datafile (obj)
  (declare (ignore obj))
  ;; (server-file-dialog obj "Open..." "./" (lambda (fname) (on-load-datafile obj fname)))
  )

(defun add-axis-to-chart (chart params)
  "Add the given axis to the CHART."
  (let* ((name (second (assoc :name params)))
         (position (second (assoc :pos params)))
         (type (second (assoc :type params)))
         (stack-weight-raw (second (assoc :stack-weight params)))
         (stack-weight-num (ignore-errors (parse-integer stack-weight-raw)))
         (stack-weight (cond
                         ((equal "" stack-weight-raw) nil)
                         (stack-weight-num stack-weight-num)
                         (t (alert-toast chart "Invalid Number" (format nil "~S is not a valid number" stack-weight-raw)))))
         (stack-raw (second (assoc :stack params)))
         (stack (if (equal "" stack-raw) nil stack-raw))
         (color (second (assoc :color params)))
         (title-raw (second (assoc :label params)))
         (title (if (equal "" title-raw) nil title-raw)))
    (clog-chart/add-axis chart name :position position :type type :stack stack :stack-weight stack-weight :color color :title title)))

(defun add-key-to-chart (chart tracer params)
  "Add the given key KEY from the TRACER to the CHART"
  (let* ((key (second (assoc :key params)))
         (color (second (assoc :color params)))
         (func-string (second (assoc :operator params)))
         (func (ignore-errors (eval (read-from-string func-string))))
         (axis (second (assoc :axis params)))
         (stepped (second (assoc :stepped params))))
    (flet ((data-function ()
             (let* ((entry (tracer-pp/get key tracer))
                    (vec-value-raw (pp-entry/value entry)))
               (values (pp-entry/time entry)
                     (cond
                       ((or (null func-string) (equal "" func-string)) vec-value-raw)
                       ((functionp func) (map 'vector func vec-value-raw))
                       (t (clog-gui:alert-toast chart "Invalid Operation"
                                                (format nil "~S is not a valid function" func-string)
                                                :time-out 3.5)))))))
      (clog-chart/add chart #'data-function
                      :label key
                      :color color
                      :stepped (if (equal "" stepped) nil stepped)
                      :axis axis)
      (clog-chart/update chart))))

(defun new-tracer-window (obj str tracer)
  "Creates an analysis window for a tracer."
  (if (typep tracer 'f:tracer-pp)
      (with-clog-window (obj :title (format nil "~A Visualizer" str))
          ((gui-menu-drop-down
            (:content "Graph")
            (gui-menu-item (:bind b-add :content "Add..."))
            (gui-menu-item (:bind b-axis :content "Add Y Axis..."))
            (gui-menu-item (:bind b-title :content "Set Graph Title..."))
            (gui-menu-item (:bind b-up :content "Update"))
            (gui-menu-item (:bind b-clr :content "Clear")))
           (gui-menu-drop-down
            (:content "View")
            (gui-menu-item (:bind but-view-fit :content "Fit"))
            (gui-menu-item (:bind but-live :content "Enable Live Data")))
           (gui-menu-drop-down
            (:content "SLY")
            (gui-menu-item (:bind but-cpy-chart :content "Copy chart object to emacs"))
            (gui-menu-item (:bind but-cpy-tracer :content "Copy tracer object to emacs"))))
          ((chart (:bind chart)))
        (set-on-click-copy but-cpy-chart "Chart" chart)
        (set-on-click-copy but-cpy-tracer "Tracer" tracer)
        (set-on-click-ps but-view-fit ((@ (clog-chart/chart chart) reset-zoom)))
        (set-on-click-obj b-up
          (clog-chart/update-datasets chart)
          (clog-chart/update chart))
        (set-on-click-obj b-clr
          (clog-chart/clear chart)
          (clog-chart/update chart))
        ;; Add key
        (let (update-semaphore thread)
          (set-on-click-obj but-live
            (if thread
                (progn
                  (bt:destroy-thread thread)
                  (setf (clog:inner-html but-live) "Enable Live Data")
                  (setf thread nil)
                  (clog-chart/update chart))
                (progn
                  (setf (clog:inner-html but-live) "Disable Live Data")
                  (multiple-value-setq (update-semaphore thread) (clog-chart/start-live-data-thread chart))))))
        (set-on-click-obj b-title
          (form-dialog obj "Select the desired graph title"
                       '(("Title" :title))
                       (lambda (results)
                         (clog-chart/set-title chart (second (assoc :title results)))
                         (clog-chart/update chart))))
        (set-on-click-obj b-add
          (form-dialog obj "Select the key to draw"
                       `(("Key" :key :select
                                ,(mapcar (lambda (x) (list x x)) (alexandria:hash-table-keys (tracer/type-map tracer))))
                         ("Color" :color :color)
                         ("Modify" :operator)
                         ("Axis" :axis :select (("Default" nil :selected)
                                                ,@(mapcar (lambda (x) (list x x)) (clog-chart/axis chart))))
                         ("Step type" :stepped  :select (("Linear Interpolation" "")
                                                         ("Stepped" t :selected)
                                                         ("Step before" "before")
                                                         ("Step middle" "middle")
                                                         ("Step after" "after")
                                                         )))
                       (lambda (results)
                         (when results
                           (add-key-to-chart chart tracer results)))))
        (set-on-click-obj b-axis
          (form-dialog obj "New Y Axis"
                       `(("Name" :name)
                         ("Position" :pos :select (("Left" "left")
                                                   ("Right" "right" :selected)))
                         ("Type" :type :select
                                 (("Linear" "linear" :selected)
                                  ("Log" "logarithmic")
                                  ("Category" "category")))
                         ("Border Color" :color :color)
                         ("Label" :label)
                         ("Stack" :stack)
                         ("Stack Weight" :stack-weight :number)
                         ;; ("Labels" :labels)
                         ;; ("Min" :min :number)
                         ;; ("Max" :max :number)
                         ;; ("Suggested Min" :sugg-min :number)
                         ;; ("Suggested Max" :sugg-max :number)
                         )
                       (lambda (results)
                         (when results
                           (add-axis-to-chart chart results))))))
      (alert-toast obj "Invalid Tracer" (format nil "~S is not a valid post-processing tracer" tracer) :time-out 3.5)))

(defun on-file-open-tracer (obj)
  (form-dialog obj "Select the tracer global variable to visualize"
               '(("Variable" "var" :text ""))
               (lambda (results)
                 (when results
                     (let* ((str (second (assoc "var" results :test #'equal)))
                            (var (ignore-errors (eval (ignore-errors (read-from-string str))))))
                       (if var
                           (new-tracer-window obj str var)
                           (alert-dialog obj (format nil "~A is not a valid tracer" str))))))
               :height 550))

;; (with-clog-create (window-content (create-gui-window obj :title "Helllllo"))
;;     (div (:content "AAAAAA"))
;;   nil)
