;; Copyright (C) 2022 gsou
;;
;; This file is part of cl-simf-lab.
;;
;; cl-simf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; cl-simf-lab is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with cl-simf-lab.  If not, see <https://www.gnu.org/licenses/>.

(in-package :clog-chart)

(defun init-chart (body &key (path-to-js "/js/"))
  "Loads the chart library"
  ;; (load-script (html-document body) (concatenate 'string path-to-js "chart.js"))
  ;; (load-script (html-document body) (concatenate 'string path-to-js "hammerjs.js"))
  ;; (load-script (html-document body) (concatenate 'string path-to-js "chartjs-plugin-zoom.min.js"))
  ;; (load-script (html-document body) (concatenate 'string path-to-js "init-chart.js"))
  ;; js lib
  (ps! body
     (setf zip-pt
           (lambda (xv yv)
             ((@ xv map) (lambda (xval i) (create x xval y (aref yv i))))))))

(defclass clog-chart (clog-div)
  ((var
    :accessor clog-chart/var
    :documentation "JS variable containing the Chart object.")
   (axis
    :accessor clog-chart/axis
    :documentation "Stores the names of the y axis of the chart"
    :initform nil)
   (dataset
    :accessor clog-chart/dataset
    :initform (make-array 0 :fill-pointer 0)
    :documentation "Contains the datasets"))
  (:documentation "Chart object"))

(defgeneric create-chart (clog-obj &key class html-id)
  (:documentation "Create a chart object"))

(defmethod create-chart ((obj clog-obj) &key (class nil) (html-id nil))
  (with-clog-create obj
      (div (:bind div :class class :html-id html-id) (canvas (:bind canvas)))
    (change-class div 'clog-chart)
    (let ((ch-obj (gensym)))
      (setf (clog-chart/var div) ch-obj)
      (ps! div
        (setf (lisp ch-obj)
              (new (-chart (lisp (html-id canvas))
                           (create
                            type "line"
                            data (create)
                            options (create
                                     plugins (create
                                              zoom (create
                                                    pan (create
                                                         enabled t
                                                         mode "xy"
                                                         modifier-key "shift")
                                                    zoom (create
                                                          wheel (create
                                                                 enabled t)
                                                          drag (create
                                                                enabled t)
                                                          mode "xy"
                                                          over-scale-mode "xy"
                                                          )))
                                     scales (create
                                             x (create
                                                type "linear"
                                                position "bottom")
                                             )))))))
      div)))

(defpsmacro lisp-undef (lisp) `(if (lisp ,lisp) (lisp ,lisp) undefined ))

(defpsmacro clog-chart/chart (obj)
  "Get the chart js variable from the object"
  `(lisp (clog-chart/var ,obj)))

(defpsmacro clog-chart/update (obj)
  "Update the given canvas"
  `((@ (clog-chart/chart ,obj) update)))

(defpsmacro clog-chart/add (obj ds)
  "Add the dataset to the chart"
  `((@ (clog-chart/chart ,obj) data datasets push)
    ,ds))

(defun clog-chart/decimation (obj param)
  (cond
    ((integerp param)
     (ps! obj (setf (@ (clog-chart/chart obj) options plugins decimation)
                    (create enabled t
                            algorithm "lttb"
                            samples (lisp param)))))
    ((stringp param)
     (ps! obj (setf (@ (clog-chart/chart obj) options plugins decimation)
                    (create enabled t
                            algorithm (lisp param)))))
    ((null param)
     (ps! obj (setf (@ (clog-chart/chart obj) options plugins decimation)
                    (create enabled nil))))
    (t (error "Invalid decimation param ~A" param))))

(defun clog-chart/update (obj)
  "Update the given canvas"
  (js-execute obj (ps (clog-chart/update obj))))

(defun clog-chart/set-title (obj str)
  "Sets the title of the chart"
  (ps! obj
    (setf (@ (clog-chart/chart obj) options plugins title)
          (create
           display t
           text (lisp str)))))

(defun clog-chart/set-type (obj typ)
  "Set the type of the chart OBJ to TYP"
  (js-execute obj (ps (setf (@ (clog-chart/chart obj) config type) (lisp typ)))))

(defun clog-chart/clear (obj)
  "Clear all the datasets in the chart."
  (setf (clog-chart/dataset obj) (make-array 0 :fill-pointer 0))
  (ps! obj (setf (@ (clog-chart/chart obj) data datasets) (array))))

(defun clog-chart/add (obj data &key label color stepped axis)
  "Add the dataset to the chart. The DATA can be either a function returning two values, the x and y points, or a list of two vectors.
If a function is used, the dataset will be recalculated when the clog-chart/update-datasets is called."
  (let* ((dat
           (trivia:match data
             ((type function) (vector-push-extend data (clog-chart/dataset obj)) (multiple-value-list (funcall data)))
             ((list _ _) (vector-push-extend :static (clog-chart/dataset obj)) data)
             (_ (error "Invalid dataset DATA: ~S" data))))
         (vec-x (car dat))
         (vec-y (cadr dat)))
    (ps! obj
      (clog-chart/add obj
                      (create
                       label (lisp-undef label)
                       background-color (lisp-undef color)
                       border-color (lisp-undef color)
                       stepped (lisp-undef stepped)
                       data (zip-pt (lisp vec-x) (lisp vec-y))
                       y-axis-i-d (lisp-undef axis))))))

(defun clog-chart/update-datasets (obj)
  "Update the dynamic datasets."
  (loop for i from 0
        for ds across (clog-chart/dataset obj)
        when (functionp ds)
          do (multiple-value-bind (data-x data-y) (funcall ds)
               (ps! obj
                 (setf
                  (@ (clog-chart/chart obj) data datasets (lisp i) data)
                  (zip-pt (lisp data-x) (lisp data-y)))))))

(defun clog-chart/add-axis (obj name &key (position "right") (type "linear") stack stack-weight color title)
  "Add a new axis to the chart"
  (declare (type string position)
           (type string  type))
  (push name (clog-chart/axis obj))
  (ps! obj
    (setf
     (@ (clog-chart/chart obj) options scales (lisp name))
     (create
      type (lisp type)
      display t
      position (lisp position)
      stack (if (lisp stack) (lisp stack) undefined)
      stack-weight (or (lisp stack-weight) undefined)
      offset (if (lisp stack-weight) t undefined)
      grid (if (lisp color)
               (create
                border-color (lisp color))
               undefined)
      title (if (lisp title)
                (create
                 display t
                 text (lisp title)
                 color (or (lisp color) undefined))
                undefined)))
    (clog-chart/update obj)))

(defun clog-chart/start-live-data-thread (chart &key (timeout 1))
  "Start a thread that continuously update the chart data. Returns a semaphore that can be used to update immediately and the thread."
  (let ((semaphore (bt:make-semaphore)))
    (values semaphore
            (bt:make-thread (lambda ()
                              (loop
                                (progn (bt:wait-on-semaphore semaphore :timeout timeout)
                                       (clog-chart/update-datasets chart)
                                       (clog-chart/update chart))))))))
