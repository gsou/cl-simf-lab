;; Copyright (C) 2022 gsou
;;
;; This file is part of cl-simf-lab.
;;
;; cl-simf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; cl-simf-lab is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with cl-simf-lab.  If not, see <https://www.gnu.org/licenses/>.

(in-package :cl-simf-lab)


;; Type

(defparameter *monitor-iface-type-field* (make-hash-table :test 'equal)
  "Contains the methods to generate the HTML controlling the simulation and showing output.")

(defmacro define-type-value (type &body body)
  `(setf (gethash ',type *monitor-iface-type-field*)
         (lambda (column iface)
           (declare (ignorable column iface))
           ,@body)))

(defun default-read-iface-value (iface)
  (typecase iface
    (interface-signal-read (interface-signal/read iface))
    (signal-dir (signal/read iface nil))))

(defun default-return-load-value (val iface)
  (lambda ()
    (unless (string= "true" (clog:jquery-query val "is(':focus')"))
      (setf (value val) (default-read-iface-value iface)))))

(define-type-value bit
  (with-clog-create column
      (form-element (:checkbox :bind val))
    (if (typep iface 'f:interface-signal-write)
        (set-on-click val (lambda (obj) (interface-signal/write iface (if (checkedp obj) 1 0))))
        (setf (disabledp val) t))
    (lambda () (setf (checkedp val) (> (default-read-iface-value iface) 0)))))

(defun load-value-type-generic (type)
  (lambda (column iface)
    (with-clog-create column
        (form-element ((if (subtypep type 'number) :number :text) :bind val))
      (if (typep iface 'f:interface-signal-write)
          (set-on-change val (lambda (obj)
                               (let* ((str (value obj))
                                      (update (ignore-errors
                                               (let ((u (read-from-string str)))
                                                 (and (typep u type) u)))))
                                 (if update
                                     (interface-signal/write iface update)
                                     (progn
                                       (setf (value val) (interface-signal/read iface))
                                       (alert-toast obj "Couldn't set value"
                                                    (format nil
                                                            "Could not set value of interface ~A to ~S as it is not valid for its type."
                                                            (signal/name iface)
                                                            str)
                                                    :time-out 2))))))
          (setf (disabledp val) t))
      (default-return-load-value val iface))))

(defun load-value-in-column (column iface type fnvec)
  (vector-push-extend
   (funcall
    (gethash type *monitor-iface-type-field*
             (load-value-type-generic type))
    column
    iface)
   fnvec))


;; Monitor window

(defun load-interface-table (table list fnvec &optional options)
  "Creates the entries in the interface table for the environment"
  (with-clog-create table
      (table-row ()
                 (table-heading (:content "Name"))
                 (table-heading (:content "Type"))
                 (table-heading (:content "Value"))
                 (table-heading (:content "Options")))
    (loop for iface in list
          do (with-clog-create table
                 (table-row ()
                            (table-column (:content (signal/name iface)))
                            (table-column (:content (format nil "~A" (signal/type iface))))
                            (table-column (:bind v))
                            (table-column (:bind opts)))
               ;; TODO Add options like messaging window, or copy iface to sly
               (load-value-in-column v iface (signal/type iface) fnvec)
               (if options (funcall options opts iface))))))

(defun new-monitor-window (obj str monitor)
  "Creates a monitoring window for a monitor object."
  (monitor/wait-init monitor)
  (let ((fnvec (make-array 0 :fill-pointer 0)))
    (with-clog-window (obj :title (format nil "~A Monitor" str))
        (
         (gui-menu-drop-down
          (:content "Simulation")
          (gui-menu-item (:content "Step" :on-click (lambda (_) (declare (ignore _)) (monitor/resume monitor :step))))
          (gui-menu-item (:content "Run" :on-click (lambda (_) (declare (ignore _)) (monitor/resume monitor :run))))
          (gui-menu-item (:content "Pause" :on-click (lambda (_) (declare (ignore _)) (monitor/resume monitor :wait)))))
         (gui-menu-drop-down
          (:bind menu-tools :content "Tools"))
         (gui-menu-drop-down
          (:content "SLY")
          (gui-menu-item (:bind but-cpy-monitor :content "Copy monitor object to emacs"))
          (gui-menu-item (:bind but-cpy-env :content "Copy environment object to emacs"))
          (gui-menu-item (:bind but-cpy-tracer :content "Copy tracer object to emacs"))))
        (
         (span (:content "Time: ")) (span (:bind time))
         (br ())
         (span (:content "State: ")) (span (:bind state))
         (br ())
         (span (:content "Time Step: ")) (text-area (:bind in-ts)) (button (:bind b-ts :content ">"))
         (br ())
         (table (:bind table))
         (br ())
         (table (:bind signals))
         )
      (let ((tracer (element-tracer/tracer (monitor/tracer monitor))))
        (when (typep tracer 'tracer-pp)
          (create-gui-menu-item
           menu-tools
           :content "Tracer Viewer"
           :on-click (lambda (obj) (new-tracer-window obj str tracer))))
        (when (typep tracer 'f::file-tracer-vcd)
          (create-gui-menu-item
           menu-tools
           :content "Open VCD in GtkWave"
           :on-click (lambda (obj) (declare (ignore obj)) (uiop:run-program (list "gtkwave" (file-tracer/path tracer))))))
        (when (typep tracer 'f::file-tracer)
          (create-gui-menu-item
           menu-tools
           :content "Open Tracer File in Emacs"
           :on-click (lambda (obj) (declare (ignore obj)) (format-emacs "(find-file ~S)" (file-tracer/path tracer))))))

      (set-on-click-copy but-cpy-monitor "Monitor" monitor)
      (set-on-click-copy but-cpy-env "Environment" (monitor/env monitor))
      (set-on-click-copy but-cpy-tracer "Tracer" (element-tracer/tracer (monitor/tracer monitor)))
      (set-on-click-obj b-ts
        (let ((i (ignore-errors (parse-integer (text-value in-ts)))))
          (if i
              (monitor/resume monitor (list :until (+ i (environment/time (monitor/env monitor)))))
              (alert-toast obj "Invalid Number" (format nil "~S is not a valid number" (text-value in-ts)) :time-out 3.5))))
      (load-interface-table table (environment/interfaces (monitor/env monitor)) fnvec)
      (load-interface-table signals (alexandria:hash-table-values (environment/signals (monitor/env monitor))) fnvec)
      (monitor/on-update (monitor :timeout 1)
        (loop for fn across fnvec when (functionp fn) do (funcall fn))
        (setf (clog:inner-html time) (format nil "~A" (environment/time (monitor/env monitor))))
        (setf (clog:inner-html state) (format nil "~A" (monitor/state monitor)))))))

(defun on-file-open-monitor (obj)
  (form-dialog obj "Select the monitor global variable to visualize"
               '(("Variable" "var" :text ""))
               (lambda (results)
                 (when results
                     (let* ((str (second (assoc "var" results :test #'equal)))
                            (var (ignore-errors (eval (ignore-errors (read-from-string str))))))
                       (if var
                           (new-monitor-window obj str var)
                           (alert-dialog obj (format nil "~A is not a valid monitor" str))))))
               :height 550))

(defun on-file-new-simulation (obj)
  (form-dialog obj "Create a new simulation"
               '(("Delta Time" :dt :text "1d-6"))
               (lambda (results)
                 (block err
                   (new-monitor-window obj "Untitled Simulation"
                                       (let* ((dt (second (assoc :dt results :test #'equal)))
                                              (int (ignore-errors (read-from-string dt))))
                                         (if (typep int 'number)
                                             (f:with-simulation-monitor
                                                 (env 'f:environment :dt int)
                                                 (tf :class 'f:tracer-pp)
                                                 () ())
                                             (progn
                                               (alert-toast obj "Invalid Number" (format nil "~S is not a valid number" dt) :time-out 3)
                                               (return-from err)))))))))
