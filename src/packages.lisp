;; Copyright (C) 2022 gsou
;;
;; This file is part of cl-simf-lab.
;;
;; cl-simf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; cl-simf-lab is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with cl-simf-lab.  If not, see <https://www.gnu.org/licenses/>.

(defpackage :cl-simf-lab-utils
  (:use :cl :clog :parenscript)
  (:export
   #:ps!
   #:ps->
   #:with-clog-window
   #:set-on-click-copy
   #:set-on-click-obj
   #:set-on-click-ps
   #:format-emacs))

(defpackage :clog-chart
  (:use :cl :clog :parenscript :cl-simf-lab-utils )
  (:export
   #:init-chart
   #:create-chart
   #:clog-chart/chart
   #:clog-chart/update
   #:clog-chart/set-type
   #:clog-chart/add
   #:clog-chart/decimation
   #:clog-chart/add-axis
   #:clog-chart/axis
   #:clog-chart/set-title
   #:clog-chart/clear
   #:clog-chart/var
   #:clog-chart/update-dataset
   #:clog-chart/update-datasets
   #:clog-chart/start-live-data-thread))

(defpackage :cl-simf-lab
  (:use :cl :cl-simf :clog :clog-chart :clog-gui :parenscript :cl-simf-lab-utils)
  (:nicknames :lab)
  (:export
   #:start
   #:element/has-view
   #:element/view
   #:create-element-view-window))

