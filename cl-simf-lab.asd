;; Copyright (C) 2022 gsou
;;
;; This file is part of cl-simf-lab.
;;
;; cl-simf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; cl-simf-lab is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with cl-simf-lab.  If not, see <https://www.gnu.org/licenses/>.

(asdf:defsystem #:cl-simf-lab
  :description "Interface for creating, editing, running and visualising CL-SIMF simulations."
  :version "0.1"
  :depends-on ("cl-simf" "clog" "parenscript" "trivia" "alexandria")
  :author "gsou"
  :license "GPL"
  :pathname "src/"
  :serial t
  :components ((:file "packages")
               (:file "utils")
               (:file "chart")
               (:file "monitor")
               (:file "viewer")
               (:file "server")))
